
## 简介

前端 UI 项目，基于 vue、vuex、avue、element-ui，用于快速构建系统中后台业务。

## 特性

- 基于现有的 element-ui 库进行的二次封装，简化一些繁琐的操作，核心理念为数据驱动视图
- 主要的组件库针对 table 表格和 form 表单场景，同时衍生出更多企业常用的组件，达到高复用，容易维护和扩展的框架
- 同时内置了丰富了数据展示组件，让开发变得更加容易

## 支持环境

现代浏览器及 IE11。

## 如何启动

```
$ cd AutoMeter
# 安装yarn并配置淘宝源
npm install -g yarn --registry=https://registry.npm.taobao.org
yarn config set registry https://registry.npm.taobao.org -g
$ yarn install
# 启动
$ yarn run serve
```
