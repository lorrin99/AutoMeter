import Layout from '@/page/index/'

export default [{
  path: '/wel',
  component: Layout,
  redirect: '/wel/index',
  children: [{
    path: 'index',
    name: '首页',
    meta: {
      i18n: 'dashboard'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/wel/index')
  }, {
    path: 'dashboard',
    name: '控制台',
    meta: {
      i18n: 'dashboard',
      menu: false,
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/wel/dashboard')
  }]
}, {
  path: '/test',
  component: Layout,
  redirect: '/test/index',
  children: [{
    path: 'index',
    name: '测试页',
    meta: {
      i18n: 'test'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/util/test')
  }]
}, {
  path: '/dict-horizontal',
  component: Layout,
  redirect: '/dict-horizontal/index',
  children: [{
    path: 'index',
    name: '字典管理',
    meta: {
      i18n: 'dict'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/util/demo/dict-horizontal')
  }]
}, {
  path: '/dict-vertical',
  component: Layout,
  redirect: '/dict-vertical/index',
  children: [{
    path: 'index',
    name: '字典管理',
    meta: {
      i18n: 'dict'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/util/demo/dict-vertical')
  }]
}, {
  path: '/info',
  component: Layout,
  redirect: '/info/index',
  children: [{
    path: 'index',
    name: '个人信息',
    meta: {
      i18n: 'info'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/system/userinfo')
  }]
}, {
  path: '/work/process/leave',
  component: Layout,
  redirect: '/work/process/leave/form',
  children: [{
    path: 'form/:processDefinitionId',
    name: '请假流程',
    meta: {
      i18n: 'work'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/work/process/leave/form')
  }, {
    path: 'handle/:taskId/:processInstanceId/:businessId',
    name: '处理请假流程',
    meta: {
      i18n: 'work'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/work/process/leave/handle')
  }, {
    path: 'detail/:processInstanceId/:businessId',
    name: '请假流程详情',
    meta: {
      i18n: 'work'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/work/process/leave/detail')
  }, {
    path: '/room/decentralized',
    name: '新增房源',
    meta: {
      i18n: 'meter'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/meter/components/room/decentralized')
  }, {
    path: '/room/centralized',
    name: '房源修改',
    meta: {
      i18n: 'meter'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/meter/components/room/centralized')
  }, {
    path: '/room/checkCost',
    name: '查看费用',
    meta: {
      i18n: 'meter'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/meter/components/room/checkCost')
  }, {
    path: '/room/costDetail',
    name: '费用明细',
    meta: {
      i18n: 'meter'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/meter/components/room/costDetail')
  }, {
    path: '/room/subhouse',
    name: '添加子房源',
    meta: {
      i18n: 'meter'
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/meter/components/room/subhouse')
  }]
}, {
  path: '/device',
  component: Layout,
  redirect: '/device/deviceLog',
  children: [{
    path: 'deviceLog',
    name: '设备日志',
    meta: {
      i18n: 'log',
      // isTab: false,
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/device/deviceLog')
  }]
}, {
  path: '/contract',
  component: Layout,
  redirect: '/contract/contractDetails',
  children: [{
    path: 'contractDetails',
    name: '合约详情',
    meta: {
      i18n: 'log',
      // isTab: false,
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/contract/contractDetails')
  }]
}, {
  path: '/contract',
  component: Layout,
  redirect: '/contract/share',
  children: [{
    path: 'share',
    name: '合约详情',
    meta: {
      i18n: 'log',
      // isTab: false,
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/contract/share')
  }]
}, {
  path: '/contract',
  component: Layout,
  redirect: '/contract/shareEditor',
  children: [{
    path: 'shareEditor',
    name: '合约编辑',
    meta: {
      i18n: 'log',
      // isTab: false,
    },
    component: () =>
      import( /* webpackChunkName: "views" */ '@/views/contract/shareEditor')
  }]
},
]
