import {
    setStore,
    getStore,
} from '@/util/store'

const room = {

    state: {
        roomStoreData: getStore({ name: 'roomStoreData' }) || {},
    },
    mutations: {
        SET_ROOM_DATA: (state, roomStoreData) => {
            state.roomStoreData = roomStoreData
            setStore({
                name: 'roomStoreData',
                content: state.roomStoreData,
                type: 'session'
            })
        },
    }
}
export default room
