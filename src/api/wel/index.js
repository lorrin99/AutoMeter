import request from '@/router/axios';

/* 获取充值金额 */ 
export const getRecharge = () => {
  return request({
    url: '/api/blade-meter/workbench/recharge',
    method: 'get'
  })
}

/* 获取电表数据 */
export const getDeviceCount = () => {
  return request({
    url: '/api/blade-meter/workbench/device-count',
    method: 'get'
  })
}

/* 获取平台流水 */
export const getEchartData = (startDate, endDate) => {
  return request({
    url: '/api/blade-meter/workbench/bill-flow',
    method: 'get',
    params: {
      startDate,
      endDate
    }
  })
}

/* 获取地图客户打点数据 */
export const getCustorList = () => {
  return request({
    url: '/api/blade-meter/customer/index-list',
    method: 'get'
  })
}
