import request from '@/router/axios'

export const getFailList = () => {
    return request({
        url: '/api/blade-meter/measurement/fail-count',
        method: 'get',
    })
}

export const getNearWeekFailList = () => {
    return request({
        url: '/api/blade-meter/measurement/near-week-fail-count',
        method: 'get',
    })
}

export const getSuccessList = () => {
    return request({
        url: '/api/blade-meter/measurement/success-rate-yesterday',
        method: 'get',
    })
}

export const getSuccessRateList = (startDate, endDate) => {
    return request({
        url: '/api/blade-meter/measurement/success-rate-interval',
        method: 'get',
        params: {
            startDate, endDate
        }
    })
}

