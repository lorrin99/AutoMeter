import request from '@/router/axios'

export const getList = (current, size, params) => {
    return request({
        url: '/api/blade-meter/urgecharge/list',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}




