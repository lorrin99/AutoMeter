import request from '@/router/axios'

export const getList = (current, size, params) => {
    return request({
        url: '/api/blade-meter/balance/list',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

export const getDetail = (id) => {
    return request({
        url: '/api/blade-meter/balance/list',
        method: 'get',
        params: {
            id
        }
    })
}

/* 催缴 */ 
export const setUrgecharge = (accountIds) => {
    return request({
        url: '/api/blade-meter/urgecharge/manual-urge',
        method: 'post',
        params: {
            accountIds
        }
    })
}
