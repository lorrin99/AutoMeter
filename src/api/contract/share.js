import request from '@/router/axios'

/**
 * -房间设备
 * @param {*} Id 
 * @param {*} roomId 
 */
export const getRoomDetail = (Id, roomId) => {
    return request({
        url: '/api/blade-meter/room/roomDeviceList',
        method: 'get',
        params: {
            Id, roomId
        }
    })
}

/**
 * -客户list
 * @param {*} name 
 */
export const getAppellation = (name) => {
    return request({
        url: '/api/blade-meter/customer/appellation',
        method: 'get',
        params: {
            name
        }
    })
}

/**
 * -客户账户
 * @param {*} customerId
 */
export const getCustomerAccountList = (customerId) => {
    return request({
        url: '/api/blade-meter/account/list',
        method: 'get',
        params: {
            customerId, size: 100, current: 1
        }
    })
}


/**
 * -用户 
 * @param {*} id 
 */
export const getCustomerMember = (id) => {
    return request({
        url: '/api/blade-meter/member/customerMember',
        method: 'get',
        params: {
            id
        }
    })
}

/**
 * -收款 
 */
export const getReceiptAccountList = () => {
    return request({
        url: '/api/blade-meter/receiptbankinfo/receiptAccountList',
        method: 'get',
    })
}


/**
 * -新增
 * @param {*} row 
 */
export const add = (row) => {
    return request({
        url: '/api/blade-meter/contract/save',
        method: 'post',
        data: row
    })
}

/**
 * -模板
 * @param {*} current 
 * @param {*} size 
 * @param {*} params 
 */
export const getTemplateList = (current, size, params) => {
    return request({
        url: '/api/blade-meter/calculationtemplate/page',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

/**
 * -详情
 * @param {*} id 
 */
export const getDetail = (id) => {
    return request({
        url: '/api/blade-meter/contract/detail',
        method: 'get',
        params: {
            id
        }
    })
}


/**
 * -修改
 * @param {*} row 
 */
export const update = (row) => {
    return request({
        url: '/api/blade-meter/contract/update',
        method: 'post',
        data: row
    })
}


/**
 * -中止合约
 * @param {*} row 
 */
export const terminate = (id) => {
    return request({
        url: '/api/blade-meter/contract/terminate',
        method: 'post',
        params: {
            id
        }
    })
}































