import request from '@/router/axios'
// 获取列表
export const getList = (current, size, params) => {
    return request({
        url: '/api/blade-meter/device/list',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

// 新增或修改
export const add = (data) => {

    return request({
        url: '/api/blade-meter/device/submit',
        method: 'post',
        data: data
    })
}

// 设备详情
export const getDetail = (sn) => {
    return request({
        url: '/api/blade-meter/device/detail',
        method: 'get',
        params: {
            sn
        }
    })
}

// 删除  
export const remove = (id) => {
    return request({
        url: '/api/blade-meter/device/remove',
        method: 'post',
        params: {
            id
        }
    })
}

// 用电明细
export const getDetailList = (params) => {
    return request({
        url: '/api/blade-meter/devicedata/detailist',
        method: 'get',
        params: {
            ...params,
        }
    })
}

// 用电明细
export const getDictionaryList = (code) => {
    return request({
        url: '/api/blade-system/dict/dictionary',
        method: 'get',
        params: {
            code
        }
    })
}


