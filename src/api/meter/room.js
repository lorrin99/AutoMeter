import request from '@/router/axios'

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-meter/room/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-meter/room/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-meter/room/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/blade-meter/room/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-meter/room/submit',
    method: 'post',
    data: row
  })
}

/* 2020/8/27 房源接口 */
/* 房源地区列表 */
export const getRoomList = (id, Level) => {
  return request({
    url: '/api/blade-meter/room/list',
    method: 'post',
    params: {
      id, Level
    }
  })
}

/* 房源小区列表 */
export const getEstateList = (data) => {
  return request({
    url: '/api/blade-meter/estate/EstateList',
    method: 'get',
    params: { ...data }
  })
}

/* 新增房源（分散式房源） */
export const saveRoom = (row) => {
  return request({
    url: '/api/blade-meter/room/save',
    method: 'post',
    data: row
  })
}
/* 新增房源（集中式房源） */


/**
 * -小区名称  
 * @param {*} row 
 */
export const getRegionName = (content) => {
  return request({
    url: '/api/blade-meter/estate/RegionName',
    method: 'get',
    params: {
      content
    }
  })
}

/**
 * -小区联动
 * @param {*} content 
 */
export const getTreeLink = (id, Level) => {
  return request({
    url: '/api/blade-meter/estate/treeLink',
    method: 'get',
    params: {
      id, Level
    }
  })
}
