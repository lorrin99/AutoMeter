import request from '@/router/axios';

export const getList = (params) => {
  return request({
    url: '/api/blade-meter/customer/page',
    method: 'get',
    params: {
      ...params
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-meter/customer/info',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-meter/customer/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/blade-meter/customer/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-meter/customer/submit',
    method: 'post',
    data: row
  })
}

export const getTreeList = (parentId) => {
  if (!parentId) {
    parentId = 0;
  }
  return request({
    url: '/api/blade-system/region/tree',
    method: 'get',
    params: {
      parentId
    }
  })
}

export const getArchives = (row) => {
  return request({
    url: '/api/blade-meter/customerfile/page',
    method: 'get',
    params: row
  })
}

export const uploadArchives = (file, customerId) => {
  return request({
    url: '/api/blade-meter/customerfile/upload-customer-file',
    method: 'post',
    data: file,
    params: {
      customerId
    }
  })
}

export const deleteArchives = (ids) => {
  return request({
    url: '/api/blade-meter/customerfile/remove',
    method: 'post',
    params: { ids }
  })
}

export const addSettlement = (row) => {
  return request({
    url: '/api/blade-meter/customer/save-customer-and-account',
    method: 'post',
    data: row
  })
}
