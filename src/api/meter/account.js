import request from '@/router/axios';

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-meter/account/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-meter/account/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-meter/account/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/blade-meter/account/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-meter/account/submit',
    method: 'post',
    data: row
  })
}

export const getCustorList = (current, size, params) => {
  return request({
    url: '/api/blade-meter/customer/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}
