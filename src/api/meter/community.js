import request from '@/router/axios'

export const getList = (current, size, params) => {
  return request({
    url: '/api/blade-meter/community/list',
    method: 'get',
    params: {
      ...params,
      current,
      size,
    }
  })
}

export const getDetail = (id) => {
  return request({
    url: '/api/blade-meter/community/detail',
    method: 'get',
    params: {
      id
    }
  })
}

export const remove = (ids) => {
  return request({
    url: '/api/blade-meter/community/remove',
    method: 'post',
    params: {
      ids,
    }
  })
}

export const add = (row) => {
  return request({
    url: '/api/blade-meter/community/submit',
    method: 'post',
    data: row
  })
}

export const update = (row) => {
  return request({
    url: '/api/blade-meter/community/submit',
    method: 'post',
    data: row
  })
}

/* 修改房源区域部分 */
export const getTreeList = () => {
  return request({
    url: '/api/blade-meter/estate/EstateTree',
    method: 'get'
  })
}
/* 区域详情 */
export const getTreeDetails = (id) => {
  return request({
    url: '/api/blade-meter/community/detail',
    method: 'get',
    params: {
      id
    }
  })
}

/**
 * 小区新增
 * @param {*} row 
 */
export const estateSave = (row) => {
  return request({
    url: '/api/blade-meter/estate/estateSave',
    method: 'post',
    data: row
  })
}

/**
 * 小区修改
 * @param {*} row 
 */
export const estateUpdate = (row) => {
  return request({
    url: '/api/blade-meter/estate/estateUpdate',
    method: 'post',
    data: row
  })
}

/**
 * 1级小区删除
 * @param {*} row 
 */
export const estateRemove = (ids) => {
  return request({
    url: '/api/blade-meter/community/remove',
    method: 'post',
    params: {
      ids
    }
  })
}

/**
 * 二三级小区删除
 * @param {*} row 
 */
export const buildingRemove = (ids) => {
  return request({
    url: '/api/blade-meter/estate/remove',
    method: 'post',
    params: {
      ids
    }
  })
}
