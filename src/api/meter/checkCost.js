import request from '@/router/axios'

// 查看费用
export const getCheckFee = (accountId) => {
    return request({
        url: '/api/blade-meter/account/checkFee',
        method: 'get',
        params: {
            accountId
        }
    })
}

// 费用明细
export const getFeeDetailList = (current, size, params) => {
    return request({
        url: '/api/blade-meter/account/feeDetail',
        method: 'get',
        params: {
            ...params,
            current,
            size,
        }
    })
}

// 线下充值
export const getRecharge = (row) => {
    return request({
        url: '/api/blade-meter/recharge/recharge',
        method: 'post',
        data: row
    })
}
