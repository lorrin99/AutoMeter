/* 业务设置 */
import request from '@/router/axios';

/* 充值提醒 短信提醒 欠费拉闸提醒 */

// 查询配置信息
export const getConfig = () => {
  return request({
    url: '/api/blade-meter/businessconfig/list',
    method: 'get'
  })
}

// 修改配置信息
export const setConfig = (params) => {
  console.log(params)
  return request({
    url: '/api/blade-meter/businessconfig/saveOrUpdate',
    method: 'post',
    data: {
      ...params
    }
  })
}
